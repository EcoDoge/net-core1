﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore1
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string[] Phones { get; set; }
        public bool IsActive { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {

            // Open database (or create if doesn't exist)
            using (var db = new LiteDatabase(@"./MyData.db"))
            {
                var col = db.GetCollection<Customer>("customers");

                // Create your new customer instance
                var customer = new Customer
                {
                    Name = "John Doe",
                    Phones = new string[] { "8000-0000", "9000-0000" },
                    IsActive = true
                };

                // Insert new customer document (Id will be auto-incremented)
                col.Insert(customer);

                // Update a document inside a collection
                customer.Name = "Joana Doe";

                col.Update(customer);

                // Index document using a document property
                col.EnsureIndex(x => x.Name);

                // Use Linq to query documents
                var result = col.Find(x => x.Name.StartsWith("Jo")).First();
                Console.WriteLine(result.Name);
            }
            Console.Read();
        }
    }
}
